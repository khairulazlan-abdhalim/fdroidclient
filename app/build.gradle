apply plugin: 'com.android.application'
apply plugin: 'witness'
apply plugin: 'checkstyle'
apply plugin: 'pmd'

/* gets the version name from the latest Git tag, stripping the leading v off */
def getVersionName = { ->
    def stdout = new ByteArrayOutputStream()
    exec {
        commandLine 'git', 'describe', '--tags', '--always'
        standardOutput = stdout
    }
    return stdout.toString().trim().substring(1)
}

repositories {
    jcenter()
}

dependencies {
    compile project(':privileged-api-lib')

    compile 'com.android.support:support-v4:23.4.0'
    compile 'com.android.support:appcompat-v7:23.4.0'
    compile 'com.android.support:support-annotations:23.4.0'

    compile 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    compile 'com.google.zxing:core:3.2.1'
    compile 'eu.chainfire:libsuperuser:1.0.0.201602271131'
    compile 'cc.mvdan.accesspoint:library:0.2.0'
    compile 'info.guardianproject.netcipher:netcipher:1.2.1'
    compile 'commons-io:commons-io:2.4'
    compile 'commons-net:commons-net:3.4'
    compile 'org.openhab.jmdns:jmdns:3.4.2'
    compile('ch.acra:acra:4.8.5') {
        exclude module: 'support-v4'
        exclude module: 'support-annotations'
    }
    compile 'io.reactivex:rxjava:1.1.0'
    compile 'io.reactivex:rxandroid:0.23.0'

    testCompile 'junit:junit:4.12'

    // 3.1-rc1 is required because it is the first to implements API v23.
    testCompile "org.robolectric:robolectric:3.1-rc1"

    testCompile "org.mockito:mockito-core:1.10.19"

    androidTestCompile 'com.android.support:support-annotations:23.4.0'
    androidTestCompile 'com.android.support.test:runner:0.5'
    androidTestCompile 'com.android.support.test:rules:0.5'
}

if (!hasProperty('sourceDeps')) {

    repositories {
        // This is here until we sort out all dependencies from mavenCentral/jcenter. Once all of
        // the dependencies below have been sorted out, this can be removed.
        flatDir {
            dirs 'libs/binaryDeps'
        }
    }

    dependencies {
        compile 'com.madgag.spongycastle:pkix:1.53.0.0'
        compile 'com.madgag.spongycastle:prov:1.53.0.0'
        compile 'com.madgag.spongycastle:core:1.53.0.0'

        // Upstream doesn't have a binary on mavenCentral/jcenter yet:
        // https://github.com/kolavar/android-support-v4-preferencefragment/issues/13
        compile(name: 'support-v4-preferencefragment-release', ext: 'aar')

        // Fork for F-Droid, including support for https. Not merged into upstream
        // yet (seems to be a little unsupported as of late), so not using mavenCentral/jcenter.
        compile(name: 'nanohttpd-2.1.0')

        // Upstream doesn't have a binary on mavenCentral, and it is an SVN repo on
        // Google Code.  We include this code directly in this repo, and have made
        // modifications that should be pushed to anyone who wants to maintain it.
        compile(name: 'zipsigner')
    }

    // Only do the libraries imported from maven repositories. Our own libraries
    // (like privileged-api-lib) and the prebuilt jars already checked into the
    // source code don't need to be here.
    dependencyVerification {
        verify = [
            'com.android.support:support-v4:a0d002465c0f611eedaaef2b2530707d2e9fb3a5c7ed66f53c556a12f714f43a',
            'com.android.support:appcompat-v7:d3d96637b0e8e61046567b8c87b667dcf3cd31c7447f651cb58d6e6e744adfba',
            'com.android.support:support-annotations:e91a88dd0c5e99069b7f09d4a46b5e06f1e9c4c72fc0a8e987e25d86af480f01',
            'com.nostra13.universalimageloader:universal-image-loader:dbd5197ffec3a8317533190870a7c00ff3750dd6a31241448c6a5522d51b65b4',
            'com.google.zxing:core:b4d82452e7a6bf6ec2698904b332431717ed8f9a850224f295aec89de80f2259',
            'eu.chainfire:libsuperuser:018344ff19ee94d252c14b4a503ee8b519184db473a5af83513f5837c413b128',
            'cc.mvdan.accesspoint:library:0837b38adb48b66bb1385adb6ade8ecce7002ad815c55abf13517c82193458ea',
            'commons-io:commons-io:cc6a41dc3eaacc9e440a6bd0d2890b20d36b4ee408fe2d67122f328bb6e01581',
            'commons-net:commons-net:38cf2eca826b8bcdb236fc1f2e79e0c6dd8e7e0f5c44a3b8e839a1065b2fbe2e',
            'info.guardianproject.netcipher:netcipher:611ec5bde9d799fd57e1efec5c375f9f460de2cdda98918541decc9a7d02f2ad',
            'org.openhab.jmdns:jmdns:7a4b34b5606bbd2aff7fdfe629edcb0416fccd367fb59a099f210b9aba4f0bce',
            'com.madgag.spongycastle:pkix:6aba9b2210907a3d46dd3dcac782bb3424185290468d102d5207ebdc9796a905',
            'com.madgag.spongycastle:prov:029f26cd6b67c06ffa05702d426d472c141789001bcb15b7262ed86c868e5643',
            'com.madgag.spongycastle:core:9b6b7ac856b91bcda2ede694eccd26cefb0bf0b09b89f13cda05b5da5ff68c6b',
            'ch.acra:acra:afd5b28934d5166b55f261c85685ad59e8a4ebe9ca1960906afaa8c76d8dc9eb',
            'io.reactivex:rxjava:2c162afd78eba217cdfee78b60e85d3bfb667db61e12bc95e3cf2ddc5beeadf6',
            'io.reactivex:rxandroid:35c1a90f8c1f499db3c1f3d608e1f191ac8afddb10c02dd91ef04c03a0a4bcda',
        ]
    }

} else {

    logger.info "Setting up *source* dependencies for F-Droid (because you passed in the -PsourceDeps argument to gradle while building)."

    dependencies {
        compile(project(':extern:support-v4-preferencefragment')) {
            exclude module: 'support-v4'
        }
        compile project(':extern:nanohttpd:core')
        compile project(':extern:zipsigner')
    }

    task binaryDeps(type: Copy, dependsOn: ':app:prepareReleaseDependencies') {

        enabled = project.hasProperty('sourceDeps')
        description = "Copies .jar and .aar files from subproject dependencies in extern/ to app/libs. Requires the sourceDeps property to be set (\"gradle -PsourceDeps binaryDeps\")"

        from('../extern/') {
            include 'support-v4-preferencefragment/build/outputs/aar/support-v4-preferencefragment-release.aar'
            include 'nanohttpd/core/build/libs/nanohttpd-2.1.0.jar'
            include 'zipsigner/build/libs/zipsigner.jar'
        }

        into 'libs/binaryDeps'
        includeEmptyDirs false

        eachFile { FileCopyDetails details ->
            // Don't copy to a sub folder such as libs/binaryDeps/Project/build/outputs/aar/project.aar, but
            // rather libs/binaryDeps/project.aar.
            details.path = details.name
        }
    }
}

android {
    compileSdkVersion 23
    buildToolsVersion '23.0.3'
    useLibrary 'org.apache.http.legacy'

    buildTypes {
        // use proguard on debug too since we have unknowingly broken
        // release builds before.
        all {
            minifyEnabled true
            useProguard true
            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            testProguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro', 'src/androidTest/proguard-rules.pro'
        }
    }

    compileOptions {
        compileOptions.encoding = "UTF-8"

        // Use Java 1.7, requires minSdk 8
        sourceCompatibility JavaVersion.VERSION_1_7
        targetCompatibility JavaVersion.VERSION_1_7
    }

    defaultConfig {
        versionCode 101000
        versionName getVersionName()
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }

    testOptions {
        unitTests {
            // prevent tests from dying on android.util.Log calls
            returnDefaultValues = true
            all {
                // All the usual Gradle options.
                testLogging {
                    events "skipped", "failed", "standardOut", "standardError"
                    showStandardStreams = true
                }
            }
        }
    }

    lintOptions {
        checkReleaseBuilds false
        abortOnError false

        htmlReport true
        xmlReport false
        textReport false

        // Our translations are crowd-sourced
        disable 'MissingTranslation'

        // We have locale folders like "values-he" and "values-id" as symlinks
        // since some devices ship deprecated locale codes
        disable 'LocaleFolder'

        // Like supportsRtl or parentActivityName. They are on purpose.
        disable 'UnusedAttribute'
    }

    packagingOptions {
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/INDEX.LIST'
        exclude '.readme'
    }
}

checkstyle {
    toolVersion = '6.19'
}

task checkstyle(type: Checkstyle) {
    configFile file("${project.rootDir}/config/checkstyle/checkstyle.xml")
    source 'src/main/java', 'src/test/java', 'src/androidTest/java'
    include '**/*.java'

    classpath = files()
}

pmd {
    toolVersion = '5.4.2'
    consoleOutput = true
}

task pmdMain(type: Pmd, dependsOn: assembleDebug) {
    ruleSetFiles = files("${project.rootDir}/config/pmd/rules.xml", "${project.rootDir}/config/pmd/rules-main.xml")
    ruleSets = [] // otherwise defaults clash with the list in rules.xml
    source 'src/main/java'
    include '**/*.java'
}

task pmdTest(type: Pmd, dependsOn: assembleDebug) {
    ruleSetFiles = files("${project.rootDir}/config/pmd/rules.xml", "${project.rootDir}/config/pmd/rules-test.xml")
    ruleSets = [] // otherwise defaults clash with the list in rules.xml
    source 'src/test/java', 'src/androidTest/java'
    include '**/*.java'
}

task pmd(dependsOn: [pmdMain, pmdTest]) {}

// This person took the example code below from another blogpost online, however
// I lost the reference to it:
// http://stackoverflow.com/questions/23297562/gradle-javadoc-and-android-documentation
android.applicationVariants.all { variant ->

    task("generate${variant.name}Javadoc", type: Javadoc) {
        title = "$name $version API"
        description "Generates Javadoc for F-Droid."
        source = variant.javaCompile.source

        def sdkDir
        Properties properties = new Properties()
        File localProps = project.rootProject.file('local.properties')
        if (localProps.exists()) {
            properties.load(localProps.newDataInputStream())
            sdkDir = properties.getProperty('sdk.dir')
        } else {
            sdkDir = System.getenv('ANDROID_HOME')
        }
        if (!sdkDir) {
            throw new ProjectConfigurationException("Cannot find android sdk. Make sure sdk.dir is defined in local.properties or the environment variable ANDROID_HOME is set.", null)
        }

        ext.androidJar = "${sdkDir}/platforms/${android.compileSdkVersion}/android.jar"
        classpath = files(variant.javaCompile.classpath.files) + files(ext.androidJar)
        options.links("http://docs.oracle.com/javase/7/docs/api/");
        options.links("http://d.android.com/reference/");
        exclude '**/BuildConfig.java'
        exclude '**/R.java'
    }
}
